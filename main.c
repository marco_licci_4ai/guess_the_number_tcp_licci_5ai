#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define TCP_PORT 5000

char randomNumber(){
  srand(time(NULL));
  return rand() % 10 + 1;
}

int main() {
  int request_socket_id;
  int communication_socket_id;
  struct sockaddr_in server_add;
  struct sockaddr_in client_add;
  size_t client_add_size;
  char buffer[1024];
  char message[1024];
  int index;
  unsigned int num;
  unsigned int randNum;
  int n;

  if((request_socket_id = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
    printf("Error opening socket!\r\n");
  }

  memset(&server_add, 0, sizeof(server_add));
  server_add.sin_family = AF_INET;
  server_add.sin_addr.s_addr = 0;
  server_add.sin_port = htons(TCP_PORT);

  if(bind(request_socket_id, (struct stockaddr *)&server_add, sizeof(server_add)) < 0){
    printf("Error binding socket!\r\n");
    close(request_socket_id);
    return -1;
  }

  if(listen(request_socket_id, 1)){
    printf("Error listening to socket!\r\n");
    close(request_socket_id);
    return -1;
  };

  printf("Server running on port %d\r\n", TCP_PORT);

  while (1) {
    client_add_size = sizeof(client_add);
    communication_socket_id = accept(request_socket_id, (struct stockaddr *)&client_add, &client_add_size);
    sprintf(message, "%s\r\n", "Guess a number between 1 and 10...");
    send(communication_socket_id, message, strlen(message), 0);

    if(communication_socket_id >= 0){
      index = 0;
      while (1) {
        if((n = recv(communication_socket_id, (void*)buffer, sizeof(buffer), 0)) <= 0){
          close(communication_socket_id);
          printf("Connection closed by client\r\n");
          break;
        }
        else {
          for (size_t i = 0; i < n; i++) {
            if(buffer[i] == '\r' || buffer[i] == '\n'){
              message[index] = '\0';
              index = 0;

              sscanf(message, "%d", &num);
              randNum = randomNumber();
              sprintf(message, "The number was %d.\r\nYou %s.\r\n", randNum,
                      (randNum == num) ? "win" : "lose");

              send(communication_socket_id, message, strlen(message), 0);
              close(communication_socket_id);
            }
            else {
              if(index >= sizeof(message)){
                index = 0;
              }
              message[index] = buffer[i];
              index++;
            }
          }
        }
      }
    }
  }
  close(request_socket_id);
  return 0;
}
